package com.ait.example.steps;

import org.springframework.beans.factory.annotation.Autowired;

import com.ait.example.MemberRepository;
import com.ait.example.ShowLevelRepository;
import com.ait.example.ShowRepository;

import cucumber.annotation.Before;

public class BeforeSteps {

    @Autowired
    private MemberRepository memberRepository;

    @Autowired
    private ShowRepository showRepository;

    @Autowired
    private ShowLevelRepository showLevelRepository;

    @Before
    public void beforeScenario() {
    	System.out.println("whqt this ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        memberRepository.deleteAll();
        showRepository.deleteAll();
        showLevelRepository.deleteAll();
    }
}
